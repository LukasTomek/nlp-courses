from sklearn.feature_extraction.text import CountVectorizer

data = [
        'Dog chases cat.',
        'Cat chases mouse.',
        'Mouse likes dog.',
        'We dont have a dog.',
        'But we have a mouse.',
        'Dog chases dog.'
        ]

vectorizer = CountVectorizer()

vectorizer.fit(data)

print(vectorizer.vocabulary_)

vector = vectorizer.transform(data)

print(vector.toarray())
